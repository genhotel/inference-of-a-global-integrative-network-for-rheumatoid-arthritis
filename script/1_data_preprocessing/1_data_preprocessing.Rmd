---
title: "GSE117769 data pre-processing"
author: "Dereck de Mézquita & Quentin Miagoux"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  html_document:
    theme: united
    highlight: tango
    keep_md: yes
    number_sections: yes
    smart: no
    toc: yes
    toc_depth: 2
    toc_float: yes
    css: "../style/style.css"
    code_folding: hide
params:
  rmd: "script/1_data_preprocessing/1_data_preprocessing.rmd"
editor_options:
  chunk_output_type: console
always_allow_html: yes
---

<a download="script/1_data_preprocessing/1_data_preprocessing.Rmd" href="`r base64enc::dataURI(file = params$rmd, mime = 'text/rmd', encoding = 'base64')`">R Markdown source file (to produce this document)</a>

```{r setup, include=FALSE}
knitr::opts_chunk$set(cache = FALSE, warning = FALSE, message=FALSE, fig.width = 7, fig.height = 7, fig.align = "center")
```

```{r}
source("script/1_data_preprocessing/common.R")

dds <- DESeqDataSetFromMatrix(countData = dataset,
                              colData = metadata_deseq,
                              design = ~ Gender + Phenotype)
dds <- DESeq(dds)

ntd <- normTransform(dds)
vsd <- vst(dds, blind=FALSE)
```

# Introduction

The dataset **GSE117769 by Goldberg et al.** is composed of ensembl_id probes in raw and individuals in column 
ensembl probes are converted to HGNC gene symbol format  

120 Individuals constitute this dataset  
101 are RA or Healthy and were extracted.  
among these 101 individuals, 2 were the same individual (replicates). We merged these two replicates by computing the mean.  
Among the 100 individuals, 5 were from Asia while the others belong to Europe. So we removed these 5 individuals.  

In this pre-processing analysis, we are analysing **95** individuals data expression and metadata.  


## Datasets informations {.tabset .tabset-fade}

### Dimension

62069 probes are composing this dataset

```{r}
dataset %>% dim
```

### Metadata dataset

```{r}
metadata %>% func_datatable
```

# Exploratory analysis 
## Metadata {.tabset .tabset-fade}

### Age

```{r Age-charts}
p<-ggplot(metadata, aes(x=Age, color=Age, fill=Age)) +
  geom_histogram(position="identity", alpha=0.5, breaks=seq(10,80,10),color="black",fill="blue") + 
  theme_bw() + 
  xlab("Age") + 
  ylab("Subject Age count") + 
  scale_x_continuous(breaks=seq(10,80,10))
p
```

### Gender

```{r Gender-charts}
metadata %>% 
  group_by(Gender) %>% 
  summarize(count=n()) %>%
  ggplot(aes(x=Gender,y=count, fill=Gender)) +
  geom_bar(stat="identity",width = 0.5,alpha=0.75, color ="black") + geom_text(
  aes(label = count, group = Gender), 
  position = position_dodge(0.8),
  vjust = -0.3, size = 5) +
  theme_bw() +
  theme(legend.position = "")
```

### Phenotype

```{r Phenotype-charts}
metadata %>% 
  group_by(Phenotype) %>% 
  summarize(count=n()) %>%
  ggplot(aes(x=Phenotype,y=count, fill=Phenotype)) +
  geom_bar(stat="identity",width = 0.5,alpha=0.75, color ="black") + geom_text(
  aes(label = count, group = Phenotype), 
  position = position_dodge(0.8),
  vjust = -0.3, size = 5) +
  theme_bw() +
  xlab("") +
  theme(legend.position = "")
```


### Age & Gender

```{r violin-age-gender-distribution}
metadata %>%
	ggplot(aes(Gender, Age, fill = Gender)) +
	geom_violin(alpha = 0.75, trim = FALSE) +
	labs(x = "", fill = "Gender") +
	geom_boxplot(width = 0.05) +
  xlab("") +
  theme_bw() +
  theme(legend.position="none") +
  scale_y_continuous(breaks=seq(0,90,by=10))
	
```


## PCA  {.tabset .tabset-fade}


PCA on normalized data [DESeq2 doc](http://bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.html#principal-component-plot-of-the-samples)

### Without labels
```{r include=FALSE}
library(genefilter)
library(ggplot2)
library(ggrepel)
library(gridExtra)

plotPCA.san <- function (object, intgroup = "condition", ntop = 500, returnData = FALSE, i,j) 
{
  rv <- rowVars(assay(object))
  select <- order(rv, decreasing = TRUE)[seq_len(min(ntop, 
                                                     length(rv)))]
  pca <- prcomp(t(assay(object)[select, ]))
  percentVar <- pca$sdev^2/sum(pca$sdev^2)
  if (!all(intgroup %in% names(colData(object)))) {
    stop("the argument 'intgroup' should specify columns of colData(dds)")
  }
  intgroup.df <- as.data.frame(colData(object)[, intgroup, drop = FALSE])
  group <- if (length(intgroup) > 1) {
    factor(apply(intgroup.df, 1, paste, collapse = " : "))
  }
  else {
    colData(object)[[intgroup]]
  }
  d <- data.frame(PC1 = pca$x[, i], PC2 = pca$x[, j], group = group, 
                  intgroup.df, name = colnames(vsd))
  if (returnData) {
    attr(d, "percentVar") <- percentVar[i:j]
    return(d)
  }
    ggplot(data = d, aes_string(x = "PC2", y = "PC3", color = "group", label = "name")) + geom_point(size = 3) + xlab(paste0("PC2: ", round(percentVar[2] * 100), "% variance")) + ylab(paste0("PC3: ", round(percentVar[3] * 100), "% variance")) + coord_fixed() + geom_text_repel(size=3) 

}


pcaData_1_2 <- plotPCA.san(vsd, intgroup=c("Phenotype", "Gender"), returnData=TRUE,i=1,j=2)
pcaData_2_3 <- plotPCA.san(vsd, intgroup=c("Phenotype", "Gender"), returnData=TRUE,i=2,j=3)
pcaData_3_4 <- plotPCA.san(vsd, intgroup=c("Phenotype", "Gender"), returnData=TRUE,i=3,j=4)
pcaData_4_5 <- plotPCA.san(vsd, intgroup=c("Phenotype", "Gender"), returnData=TRUE,i=4,j=5)
pcaData_5_6 <- plotPCA.san(vsd, intgroup=c("Phenotype", "Gender"), returnData=TRUE,i=5,j=6)
pcaData_6_7 <- plotPCA.san(vsd, intgroup=c("Phenotype", "Gender"), returnData=TRUE,i=6,j=7)
pcaData_7_8 <- plotPCA.san(vsd, intgroup=c("Phenotype", "Gender"), returnData=TRUE,i=7,j=8)
pcaData_8_9 <- plotPCA.san(vsd, intgroup=c("Phenotype", "Gender"), returnData=TRUE,i=8,j=9)
pcaData_9_10 <- plotPCA.san(vsd, intgroup=c("Phenotype", "Gender"), returnData=TRUE,i=9,j=10)

my_pca <- function(x,i,j){
percentVar <- round(100 * attr(x, "percentVar"))
plot <- ggplot(x, aes(PC1, PC2, color=Phenotype, shape=Gender, label = name)) +
  geom_point(size=3) +
  xlab(paste0("PC",i,": ",percentVar[1],"% variance")) +
  ylab(paste0("PC",j,": ",percentVar[2],"% variance")) +
  theme_bw() +
  coord_fixed() + 
  geom_text_repel(data = subset(x, name %in% c("SN422","V3","SN293","SN448","SN258")),size = 5, aes(label = name))
return(plot)
}



plot1 <- my_pca(pcaData_1_2,1,2) + theme(legend.position = "none")
plot2 <- my_pca(pcaData_2_3,2,3) + theme(legend.position = "none")
plot3 <- my_pca(pcaData_3_4,3,4) + theme(legend.position = "none")
plot4 <- my_pca(pcaData_4_5,4,5) + theme(legend.position = "none")
plot5 <- my_pca(pcaData_5_6,5,6) + theme(legend.position = "none")
plot6 <- my_pca(pcaData_6_7,6,7) + theme(legend.position = "none")
plot7 <- my_pca(pcaData_7_8,7,8) + theme(legend.position = "none")
plot8 <- my_pca(pcaData_8_9,8,9) + theme(legend.position = "none")
plot9 <- my_pca(pcaData_9_10,9,10) + theme(legend.position = "none")

### All axes

grid.arrange(plot1, plot2, plot3, plot4, plot5, plot6, plot7,plot8,plot9, ncol=3)
```

```{r}
pcaData <- plotPCA(vsd, intgroup=c("Phenotype", "Gender"), returnData=TRUE) %>% mutate(Gender = if_else(Gender=="male","Male","Female"), Phenotype = if_else(Phenotype=="Healthy","Healthy","Rheumatoid Arthritis"))
percentVar <- round(100 * attr(pcaData, "percentVar"))
ggplot(pcaData, aes(PC1, PC2, color=Phenotype, shape=Gender, label = name)) +
  geom_point(size=3) +
  xlab(paste0("PC1: ",percentVar[1],"% variance")) +
  ylab(paste0("PC2: ",percentVar[2],"% variance")) +
  theme_bw() +
  coord_fixed()
```

### With labels

```{r }
ggplot(pcaData, aes(PC1, PC2, color=Phenotype, shape=Gender)) +
  geom_point(size=3) +
  xlab(paste0("PC1: ",percentVar[1],"% variance")) +
  ylab(paste0("PC2: ",percentVar[2],"% variance")) +
  theme_bw() +
  coord_fixed() + 
  geom_text_repel(data = subset(pcaData, name %in% c("SN422","V3","SN293","SN448","SN258")),size = 5, aes(label = name), show.legend = FALSE)
```



## Pheatmap {.tabset .tabset-fade}  

Pheatmaps of multiple data transformations, that are  explained [here](http://bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.html#data-quality-assessment-by-sample-clustering-and-visualization)  

### Normalized data

```{r fig.width=14, fig.height= 7.5, fig.align="center"}

select <- order(rowMeans(counts(dds,normalized=TRUE)),
                decreasing=TRUE)[1:20]
df <- as.data.frame(colData(dds)[,c("Phenotype","Gender")])
pheatmap(assay(ntd)[select,], cluster_rows=FALSE, show_rownames=FALSE,
         cluster_cols=FALSE, annotation_col=df)


```

### Variance stabilizing transformation

```{r fig.width=14, fig.height= 7.5, fig.align="center"}
pheatmap(assay(vsd)[select,], cluster_rows=FALSE, show_rownames=FALSE,
         cluster_cols=FALSE, annotation_col=df)
```


## Metadata of the identified outliers

```{r}
metadata_outliers <- metadata %>% filter(Donor %in% c("SN422","V3","SN293","SN448","SN258")) %>% dplyr::select(Donor,Phenotype,Age,Gender,Ancestry,Arthropathymeds, Biologics, Leflumide, Nsaids, Othermeds, Rituximab, Steroids, Sulfasalazine)  %>% mutate_all(~str_replace(.,"--",""))

metadata_outliers$All_medics <- metadata_outliers %>% apply(1,FUN=function(x){ x[6:length(x)] %>% unlist %>% unique %>% paste(collapse = " ")})

metadata_outliers %>% dplyr::select(Donor, Phenotype, Age, Gender, Ancestry, All_medics) %>% datatable()

metadata <- metadata %>% filter(!(Donor %in% c("SN422","V3","SN293","SN448","SN258"))) 
dataset <- dataset %>% dplyr::select(!c("SN422","V3","SN293","SN448","SN258")) 

if(!file.exists("data/GSE117769/1_data_preprocessing/pre_processed_90/metadata_preprocessed_90.csv")) {
	write.csv(metadata,"data/GSE117769/1_data_preprocessing/pre_processed_90/metadata_preprocessed_90.csv", row.names = TRUE)
}
if(!file.exists("data/GSE117769/1_data_preprocessing/pre_processed_90/expression_data_preprocessed_90.csv")) {
	write.csv(dataset,"data/GSE117769/1_data_preprocessing/pre_processed_90/expression_data_preprocessed_90.csv",row.names = TRUE)
}

```



# Conclusion

In this pre-processing analysis, we conclude that 5 individuals are outliers, that will be removed.  

Thus, **of this dataset 90 samples were selected for further analysis**

```{r}
metadata_deseq <- metadata  %>%
	dplyr::select(Gender,Phenotype, Donor) %>%
	mutate(Phenotype = str_replace_all(Phenotype," ", "_")) %>%  
	mutate_all(as.factor) %>% 
	column_to_rownames(var="Donor") 

dds <- DESeqDataSetFromMatrix(countData = dataset,
                              colData = metadata_deseq,
                              design = ~ Gender + Phenotype)

keep <- rowSums(counts(dds)) >= 10
dds <- dds[keep,]
dds <- DESeq(dds)
res <- results(dds)
res_dt <- res %>% as.data.frame()

res_dt <- res_dt %>% ensembl_func()

if(!file.exists(here("data/GSE117769/1_data_preprocessing/pre_processed_90/expression_data_normalized.csv"))) {
counts(dds, normalized = T) %>% 
	as.data.frame %>% 
	ensembl_func %>% 
	write.csv(file=here("data/GSE117769/1_data_preprocessing/pre_processed_90/expression_data_normalized.csv"))
}
```



```{r volcano, eval=FALSE, fig.height=8.5, fig.width=7, include=FALSE}
## Volcano Plot
# 
# library("EnhancedVolcano")
# Many genes are over-expressed, while few are under-expressed
# EnhancedVolcano(res_dt,
#     lab = rownames(res_dt),
#     title = '',
#     subtitle = '',
#     x = 'log2FoldChange',
#     y = 'padj',    
#     pCutoff = 0.05,
#     FCcutoff = 0.58,
#     legendLabels = c('NS', expression(Log[2]~FC),'p-adj-value', expression('p-adj-value'~and~log[2]~FC))
#   )
```
