# RA map extraction with matching TF from CoRegNet

**You cannot do the following instructions if you do not have any account on minerva**  
If it is the case, we provide the extracted file here 'data/GSE117769/3_global_network_inference/3_1_ra_map_extraction/export_latest.xml'

## RA map

Go on Minerva here  https://ramap.uni.lu/minerva/index.xhtml?id=laest_tnfaip3_13jan_2021

## TF overlay
Then, on the top left choose the menu with the three bars, and select 'plugins'.
On URL copy paste 'https://minerva-dev.lcsb.uni.lu/plugins/streamexport/plugin.js' and click on 'LOAD'.

Click on overlay on the top left and select add overlay on the bottom left.
Then click on browse to upload the file named 'TF_005.txt' from 'data/GSE117769/2_network_inference_coregnet'.
Select View, matching TF will show up in green.
Select each gene one by one that are either in Cytoplasm or Extracellular space.
Then on the very right menu, select Direction: Upstream, Modifiers: Non-blocking and Format CellDesigner SBML.
Click on Export, you should obtain a file similar to 'data/GSE117769/3_global_network_inference/3_1_ra_map_extraction/export_latest.xml'
