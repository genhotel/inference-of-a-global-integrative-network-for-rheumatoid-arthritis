# Requirements
Cytoscape file containing all networks open available here : "data/all_cytoscape_networks/all_cytoscape_networks.cys"
Cytoscape BiNoM plugin installed

# Instructions

- Open the cytoscape file and go on the network named "Global Ra network"
- Select TNF, IL6 and TGFB1
- On the top bar select BiNoM -> Graph Algorithms -> Select downstream neighbours ; Redo this step until a TF is reached
- Finally, export the subnetwork using the top bar BiNoM -> BiNoM I/O -> CellDesigner/SBML -> Create CellDesigner file from the current network...


The subnetwork extracted is also available here: data/modeling_and_simulation/subnetwork/subnetwork_casqed.sbml
