# Requirements
CellDesigner
Casq 0.9.11

# Instructions

- Open the subnetwork from the 5_1 step with CellDesigner
- Remove the two coregulations between JUN, JUND and FOS, JUND and save it
- On Linux in the folder where the file is saved and adapt the following command 'casq input output'

The subnetwork without coregulations is also available here: data/modeling_and_simulation/subnetwork_casqed/subnetwork_without_coreg.xml
The subnetwork casqed is also available here: data/modeling_and_simulation/subnetwork_casqed/subnetwork_casqed.sbml
