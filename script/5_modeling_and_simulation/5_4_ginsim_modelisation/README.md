# Requirements
GinSim 3.0.0b-SNAPSHOT nightlty build  (provided in this folder)
Java JDK

# Instructions

- Sanitize the file by on the terminal by adaptin the following command :
java -jar GINsim-3.0.0b-SNAPSHOT-jar-with-dependencies.jar -lm input.sbml -m sanitize output_sanitized.sbml
- Double Click on the ginsim jar file to launch it
- Click on import -> smbl-QUAL and select your sbml file sanitized
- To reproduce the results obtained with Ginsim in this study, and read the explanations in the paper  Miagoux, Q. et al 2021 Inference of an integrative, executable network for Rheumatoid Arthritis combining data-driven machine learning approaches and a state-of-the-art mechanistic disease map



Already sanitized file is available here : data/modeling_and_simulation/subnetwork_sanitized/subnetwork_sanitized.sbml
The ginsim file shown in the paper is available here : data/modeling_and_simulation/subnetwork_casqed/subnetwork_ginsim.zginml

The reduced ginsim file shown in the paper is available here : data/modeling_and_simulation/subnetwork_casqed/subnetwork_ginsim_reduced.zginml
