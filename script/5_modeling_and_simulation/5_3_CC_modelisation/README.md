# Requirements
CellCollective account


# Instructions

- Upload the file data/modeling_and_simulation/subnetwork_casqed/subnetwork_casqed.sbml on CellCollective (CC)
- To reproduce the results obtained with CC in this study, use the simulation tab, and read the explanations in the paper  Miagoux, Q. et al 2021 Inference of an integrative, executable network for Rheumatoid Arthritis combining data-driven machine learning approaches and a state-of-the-art mechanistic disease map
