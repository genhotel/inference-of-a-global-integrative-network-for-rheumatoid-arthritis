# Inference of an integrative, executable network for Rheumatoid Arthritis combining data-driven machine learning approaches and a state-of-the-art mechanistic disease map

This repository contains all data and materials from the following study
> Miagoux, Q. et al. [Inference of an integrative, executable network for Rheumatoid Arthritis combining data-driven machine learning approaches and a state-of-the-art mechanistic disease map]

![image](data/figure/workflow_final.png)
